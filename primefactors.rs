use std::env;

fn main() {
    println!("Hello, world!");
    let args: Vec<String> = env::args().collect();
    
    let input = &args[1];

    let mut n: u64 = input.parse::<u64>().unwrap();
    let mut i: u64 = 2;

    let mut factors = Vec::new();

    while i * i <= n {
    	if n % i != 0 {
    		i = i + 1;
    	} else {
    		n = n / i;
    		println!("i: {0}, n: {1}", i, n);
    		factors.push(i);
    	}
    }

    if n > 1 {
    	factors.push(n);
    }

    println!("[{}]", factors.iter().fold(String::new(), |acc, &num| acc + &num.to_string() + ", "));
}